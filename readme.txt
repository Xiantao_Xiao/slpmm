
Software: MATLAB R2020a. 

Simulation platform in the paper https://arxiv.org/abs/2106.11577:
a desktop computer with Intel(R) Xeon(R) E-2124G 3.40GHz and 32GB memory.

There are three numerical experiments.

1. Neyman-Pearson classification

in folder ./NP_classification, run test_NP_classification_logloss.m

2. Stochastic quadratrically constrained quadratical programming

in folder ./QCQP, run test__QCQP.m

3. Second-order stochastic dominance constrained portfolio optimization

in folder ./SSD, run test_portfolio_SSD.m


Remark: 
The elapsed cpu time of the experiments is much longer than that is shown in the figures (pure time of the algorithms). The reason is that we have to track the true values of objective and constraint functions at each iteration to show the performance of the algorithms, thus the code is very time-consuming.  Each pure time shown in the figures equals to the total time minus the time for computing  the true values of objective and constraint functions.
