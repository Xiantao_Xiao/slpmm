function [f_g_handle] = make_f_g_empirical(H_bar,C,n,p,sigma,r)
    f_g_handle.get_f    = @get_f;
    f_g_handle.get_g    = @get_g;
    f_g_handle.get_g_s    = @get_g_s;
    f_g_handle.get_g_grad_s    = @get_g_grad_s;    
    f_g_handle.get_multipliers    = @get_multipliers;
    f_g_handle.get_AL    = @get_AL;
    
     f_g_handle.antennas = n;
     f_g_handle.users = p;
     f_g_handle.H_bar = H_bar;
     f_g_handle.C = C;
     f_g_handle.sigma = sigma;
     f_g_handle.r = r;
     
    
    % the following 6 functions are from https://arxiv.org/abs/1801.08266.
    f_g_handle.line_func    = @line_func;
    f_g_handle.line_func1    = @line_func1; 
    f_g_handle.feas_check_online    = @feas_check_online; 
    f_g_handle.F_func    = @F_func; 
    f_g_handle.gnc_func    = @gnc_func;     
    f_g_handle.const_func    = @const_func;     
    
    
    
    function [ y ] = get_f( Q )
            y = 0;
            for i=1:p
                    y = y + trace(Q(:,:,i));
            end
    end

    function [ g ] = get_g( Q)
            g = zeros(p,1);
            samples = 500;
            for j = 1 : samples
                e = inf(n,p);
                for k = 1 : p
                    e(:,k) = (C{k})^(1/2)*randn(n,1) + (C{k})^(1/2)*randn(n,1)*1i;
                end
                H = H_bar + e;
                 [ A , B ] = line_func( Q, H );
               g = g + log(A./B);
            end
            g = r - g/samples;

    end

        function [ g ] = get_g_s( Q, etlt, s) 
            g = zeros(p,1);
            samples = s;
            for s = 1 : samples
                e = etlt(:,:,s);
                H = H_bar + e;
                 [ A , B ] = line_func( Q, H );
               g = g + log(A./B);
            end
            g = r - g/samples;
    end

    function [V] = get_g_grad_s(Q, etlt, s)
        V = zeros(n,n,p,p);
        samples = s;
            for s = 1 : samples
                e = etlt(:,:,s);
                H = H_bar + e;
                 [ A , B ] = line_func( Q, H );
                 for k = 1:p
                     Hk = H(:,k)*H(:,k)';
                     tmp = Hk/A(k);
                     for j = 1:p
                         V(:,:,j,k) = V(:,:,j,k) - tmp;
                         if j~=k
                           V(:,:,j,k) = V(:,:,j,k) + Hk/B(k);  
                         end
                     end
                 end
               
            end
            V = V / samples;
    end

    function [lambda_new] = get_multipliers(X,G,V,lambda,sigma0)

       % lambda_new = zeros(p,1);
        for k = 1:p
            for j = 1:p
                tmp0 = trace(real(V(:,:,j,k)'*X(:,:,j)));
            end
            tmp = lambda(k) + sigma0 * (G(k)+ tmp0);
            %tmp = max(tmp,0);
            lambda_new(j) = tmp;
        end        
    end

    function [L] = get_AL(R, Q, G, V, lambda, sigma0, alpha)
        L = real(get_f(R));
        X = R - Q;
        alpha = 0.5*alpha;
        for j = 1:p
            L = L + alpha*  sum(sum_square_abs(X(:,:,j)));
        end

        lambda_new = get_multipliers(X,G,V,lambda,sigma0);
        y = sum(pow_pos(lambda_new,2));
        L = L + y/(2*sigma0);
    end


    function [ g ] = feas_check_online( Q, etlt, n)
            g =     get_g_s( Q, etlt, n);
    end



    function [ A , B ] = line_func( Q, H )
        % A is the whole received signal => signal + noise + interference
        % B is the unwanted part of received signal => nise + interference
        tmp = zeros(size(Q(:,:,1))); 
        tmp1 = inf(p,1);
        for i = 1 : p
               tmp = tmp + Q(:,:,i);
               tmp1(i) = H(:,i)' * Q(:,:,i) * H(:,i);    
        end
        A = real(diag (H' * tmp * H) + sigma);
        B = A - real( tmp1);
    end


    function [ A ] = line_func1( Q, H )
        % A is the whole received signal => signal + noise + interference
        tmp = zeros(size(Q(:,:,1))); 
        for i = 1 : p
               tmp = tmp + Q(:,:,i);
        end
        A = real(diag (H' * tmp * H) + sigma);
    end


    function [F] = F_func(F, Q, H, ro)
        [ A , B ] = line_func( Q, H );
        for i = 1 : p
            for j = 1 : p       
                  if i ~= j
                    F{i,j} = (1-ro) * F{i,j} +...
                                ro * (H(:,i)' * Q(:,:,i)* H(:,i)) * H(:,i)*H(:,i)' / (A(i)*B(i));
                 else
                    F{i,j} = (1-ro) * F{i,j} +...
                                -ro * H(:,i)*H(:,i)' / A(i);
                  end
            end
        end
    end


    function [ y  ] = gnc_func( R, Q, H )
        % gradient of non-convex 4th term in \eqref{12}
        X = R - Q;
        [ ~ , B ] = line_func( Q, H );
        W = sum(X,3);
      %  y = zeros(p,1);
        for i = 1 : p
            y(i) = trace(real(H(:,i) * H(:,i)' * (W - X(:,:,i))/B(i)));
        end
        y = y';
    end

    function [ y ] = const_func( X, F, ro)

        tau=0.5 * ones(p,1);
%     if n < 25
%     tau = 0.1 * ones(K,1);
%     elseif n<50
%         tau = 0.25 * ones(K,1);
%     elseif n<75
%         tau = 0.5 * ones(K,1);
%     elseif n <100 
%         tau=0.75* ones(K,1);
%     else
%         tau=1* ones(K,1);
%     end;
 %   W = zeros(p,p);
    for i = 1: p
        for j = 1 : p
            
            W(i,j) = (1-ro) * trace(real(F{i,j}' * X(:,:,j)))+...
                     tau(i) * sum(sum_square_abs(X(:,:,j)));
            
        end
    end    
    y = sum(W,2);    
    end


%     function [f] = get_f(x)
%         tmp = R_matrix * x;
%         f = -mean(tmp);
%     end
% 
%     function [g] = get_g(x,j)
%         r   = R_matrix * x;
%         tmp = max(0,Y_bench(j)-r) - max(0,Y_bench(j)-Y_bench);
%         g = mean(tmp);
%     end
% 
% 
% 
%   
% 
%     
%     function [g] = get_s_g(x,j)
%         if isempty(R_sample_g)
%             error('sample not initialized');
%         end
%         r   = R_sample_g * x;
%         tmp = max(0,Y_bench(j)-r) - max(0,Y_bench(j)-Y_sample_g);
%         g = mean(tmp);
%     end
% 
% 
%     function [p] = proj(y)
%         p = proj_capped_splx(y,ub);
%     end
% 
% 
% 
%     function [] = perm_g()
%         rand_perm = randperm(size(R_matrix,1));
%         R_perm_g  = R_matrix(rand_perm,:);
%         Y_perm_g  = Y_bench(rand_perm);
%     end
% 
%     function [] = perm_f()
%         rand_perm = randperm(size(R_matrix,1));
%         R_perm_f = R_matrix(rand_perm,:);
%     end
% 
%     function [] = perm_I()
%         perm_Ind = randperm(size(R_matrix,1));
%     end
%     
% 
%     function [] = resample_g(sample_start_pos, ssize)
%         if isempty(R_perm_g)
%             error('samples are not permutated');
%         end
%         R_sample_g = R_perm_g(sample_start_pos:sample_start_pos+ssize-1,:);
%         Y_sample_g = Y_perm_g(sample_start_pos:sample_start_pos+ssize-1);
%     end
% 
% 
%     function [] = resample_f(sample_start_pos, ssize)
%         if isempty(R_perm_f)
%             error('samples are not permutated');
%         end
%         R_sample_f = R_perm_f(sample_start_pos:sample_start_pos+ssize-1,:);
%     end
% 
%     function [sample_Ind] = resample_I(sample_start_pos, ssize)
%         if isempty(perm_Ind)
%             error('samples are not permutated');
%         end
%         sample_Ind = perm_Ind(sample_start_pos:sample_start_pos+ssize-1);
%     end
%     
%     function [grad] = get_s_grad_g(x,j)
%         if isempty(R_sample_g)
%             error('sample not initialized');
%         end
%         ind    = Y_bench(j) - R_sample_g * x > 0;
%         cache  = -R_sample_g .* ind;
%         grad   = mean(cache,1)';
%     end
% 
%     function [grad] = get_grad_g(x,j)
%         ind    = Y_bench(j) - R_matrix * x > 0;
%         cache  = -R_matrix .* ind;
%         grad   = mean(cache,1)';
%     end    
%     
%     function [grad] = get_s_grad_f(x)
%         if isempty(R_sample_f)
%             error('sample not initialized');
%         end
%         grad   = -mean(R_sample_f,1)';
%     end
% 
%     function [grad] = get_grad_f(x)
%         grad   = -mean(R_matrix,1)';
%     end 
% 
% 
% 
%     function [L] = get_AL(x,J,mu,rho)
%         r = R_matrix * x;
%         L = -mean(r);
%         for i =1:length(J)
%             j = J(i);
%             tmp = max(0,Y_bench(j)-r) - max(0,Y_bench(j)-Y_bench);
%             delta = mean(tmp);
%             tmp0 =max(0, delta+mu(i)/rho);
%             L = L + 0.5 * rho * tmp0^2;
%         end
%     end
 

    
end