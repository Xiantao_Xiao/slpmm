clear all;

n = 100; % dimension
p = 10; % number of quadratical constraints
R = 10;
x_hat = -R/sqrt(n) + (2*R/sqrt(n))*rand(n,1);
% computing xstar by CVX
%cvx_solver SDPT3
    cvx_begin quiet
            variables x(n)
            minimize square_pos(norm(x))
            subject to
                for i = 1:p
                    0.5*(square_pos(norm(x))-square_pos(norm(x_hat)))-i <= 0
                end
                norm(x) <= R
    cvx_end
    xstar = x;