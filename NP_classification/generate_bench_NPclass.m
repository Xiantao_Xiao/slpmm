function [x_bench,obj_bench,cons_bench] = generate_bench_NPclass(f_g_handle,x0)

% parameter setup
    problem.record              = 0;

    problem.maxit               = 200; 
    problem.interval            = floor(problem.maxit/10);
    problem.A1                  = f_g_handle.A1;
    problem.A0                  = f_g_handle.A0; 
    problem.get_grad            = @f_g_handle.get_grad;
    problem.get_obj             = @f_g_handle.get_obj; 
    problem.proj                = @f_g_handle.proj;
    problem.lambda              = f_g_handle.lambda;
    problem.risk_level          = f_g_handle.risk_level;
    problem.b0                  = ones(size(problem.A0,1),1); 
    problem.b1                  = -ones(size(problem.A1,1),1); 
    
%  call algorithms gd_cons1 and gd_cons2
    x1                  = gd_cons2(problem,x0);
    problem.maxit       = floor(problem.maxit/10);
    x_bench             = gd_cons1(problem,x1);
    
%     x_bench             = gd_cons2(problem,x0);
    
    obj_bench           = problem.get_obj(problem.A0,problem.b0,x_bench);
    cons_bench          = problem.get_obj(problem.A1,problem.b1,x_bench) - problem.risk_level;
    
    end 
    
    
    % compute the optimal point
    function [x] = gd_cons1(problem,x0)
    
    maxit           = problem.maxit;  
    interval        = problem.interval;
    A1              = problem.A1;
    A0              = problem.A0;
    b0              = problem.b0;
    b1              = problem.b1;

    if ~isfield(problem,'proj'); proj = 0; else; proj = 1; end
    if ~isfield(problem,'prox'); prox = 0; else; prox = 1; end
    
    x = x0;
    eps = 1e-4;
    for i = 1:maxit
        cons = problem.get_obj(A1,b1,x) - problem.risk_level;
        if cons <= eps 
           grad_f = problem.get_grad(A0,b0,x);
           grad   = grad_f/norm(grad_f)^2;
           step   = eps;
        else
           grad_g = problem.get_grad(A1,b1,x);
           grad   = grad_g/norm(grad_g)^2;
           step   = cons;
        end        
        x = x - step * grad;
        if cons <= eps && prox;  x = problem.prox(x); end
        if proj;  x = problem.proj(x); end
        
        if problem.record && mod(i,interval) == 0
               obj = problem.get_obj(A0,b0,x);
                cons = problem.get_obj(A1,b1,x) - problem.risk_level;
                fprintf('Iter:%5d| obj_value is: %2.4e; constraint value is: %2.4e\n',i,obj,cons); 
        end        
        
    end
    x_optim_1 = x;    
    obj = problem.get_obj(A0,b0,x_optim_1);
    cons = problem.get_obj(A1,b1,x_optim_1) - problem.risk_level;
    if problem.record
    fprintf('\nFinal:      obj_value is: %2.4e; constraint value is: %2.4e\n',obj,cons);
    fprintf('\n');
    end
    end     
    
    % compute the optimal point
    function [x] = gd_cons2(problem,x0) 
    
    maxit       = problem.maxit; 
    interval    = problem.interval;
    
    if ~isfield(problem,'proj'); proj = 0; else; proj = 1; end
    if ~isfield(problem,'prox'); prox = 0; else; prox = 1; end
    
    eps         = 1e-2;
    x           = x0;
    A1          = problem.A1;
    A0          = problem.A0;
    b0           = problem.b0;
    b1           = problem.b1;
    obj_old     = 9999; x_old = x;
    cons_old    = 9999;
    for i = 1:maxit
          cons          = problem.get_obj(A1,b1,x) - problem.risk_level;
                                     
          step_f        = max(1/(1+i),eps);
          grad_f        = problem.get_grad(A0,b0,x);
          grad_f        = grad_f/norm(grad_f)^2;
          y             = x - step_f * grad_f;

          if proj;  y = problem.proj(y); end   
               
           if cons > eps 
               grad_g   = problem.get_grad(A1,b1,x);
               grad_g   = grad_g/norm(grad_g)^2;
               step_g   = cons;

                x       = y - step_g * grad_g;
                if proj;  x = problem.proj(x); end
           else 
               x = y;
               if prox;  x = problem.prox(x); end  
           end
           
          cons          = problem.get_obj(A1,b1,x) - problem.risk_level;
          obj           = problem.get_obj(A0,b0,x);
          if i > 20 && mod(i,10) == 0 && (obj > 1.2 * obj_old || cons > 5 * abs(cons_old) || min((obj-obj_old)/obj_old,(cons-cons_old)/abs(cons_old)) >= -eps )
              x = x_old; break; 
          end
              x_old = x; obj_old = obj;  cons_old = cons;  

           % print 
              if mod(i,interval) == 0   && problem.record 
                    fprintf('Iter: %5d| obj_value is: %2.4e; constraint value is: %2.4e\n',i,obj,cons);
               end            
   
    end
        
        x_optim = x;
        obj_optim = problem.get_obj(A0,b0,x_optim);
        cons_optim = problem.get_obj(A1,b1,x_optim) - problem.risk_level;
        if problem.record 
        fprintf('\nFinal:       obj_value is: %2.4e; constraint value is: %2.4e\n',obj_optim,cons_optim);
        fprintf('\n'); 
        end
    end   
    
    
    
%     problem.dim = size(A0,2); problem.samples = 1; 
%     problem.cost = @(x) f_g_handle.get_obj(A0,b(b==1),x);
%     problem.full_grad = @(x) f_g_handle.get_grad(A0,b(b==1),x); 
% 
%     
%     opts = struct('step_alg', 'backtracking','w_init',zeros(size(A0,2),1),'proj',1);
%     x_initial_2 = sd_nesterov(problem,opts);
    

    

    

    

    

    